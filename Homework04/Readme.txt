Thiết kế chi tiết lớp
Bài tập cá nhân: Vẽ các biểu đồ trình tự cho các lớp thiết kế trong use case mình phụ trách. Có
thể cần vẽ nhiều biểu đồ, mỗi biểu đồ ứng với một scenario trong use case. Dựa trên các biểu
đồ trình tự đã vẽ, vẽ biểu đồ lớp thiết kế cho use case mình phụ trách.
Bài tập nhóm: Gộp lại các biểu đồ lớp thiết kế của mỗi thành viên, tổ chức thành các package
cho hợp lý, thống nhất cách thức đặt tên.
Sau đó, vẽ biểu đồ package cho toàn nhóm và cho từng cá nhân (cần phân thành các tầng)

-------

Phân công nhiệm vụ giống như vẽ use case chi tiết !
Phần này, nhóm mình cứ làm cá nhân theo như phân công từ trước, sau đó push lên thư mục cá nhân và thư mục Group, 
mình sẽ nhắc lại công việc:

Hồng: Thêm xe cho bãi ( admin)
Thu: Tìm kiếm bãi xe + xem chi tiết xe trong bãi (user)
Đông: Thay đổi thông tin bãi xe ( admin)
Huy: Quản lí xe đang thuê  (admin)
Quang: Thuê xe ( user)
Sơn: Trả xe ( user)
Giang: Thay đổi thông tin xe ( admin)

//Đông
