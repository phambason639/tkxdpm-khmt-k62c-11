Vẽ biểu đồ use case (bao gồm biểu đồ tổng quan và các biểu đồ phân rã nếu có)
Mỗi thành viên chọn một use case nghiệp vụ (bỏ qua các use case đăng nhập đăng ký) để thực
hiện. Cần chỉ rõ ai làm use case nào. Trừ khi không còn use case, sinh viên không được phép
chọn các use case dễ (xóa/xem dữ liệu). Mỗi thành viên sẽ làm việc xuyên suốt với use case
này. Trong bài tập 1, mỗi thành viên cần đặc tả use case mình phụ trách, đồng thời cả nhóm
hoàn thành tài liệu SRS theo mẫu trong thư mục Dropbox.
--------------------
Vẽ biểu đồ use case tổng quan !
Nhóm em đã thống nhất !
Về phần tài liệu đặc tả SRS, nhóm đã thống nhất sẽ chủ yếu tập trung đặc tả Usecase.

Ý tưởng tạo use case:
Dựa vào các hành động của người dùng
Người dùng thường sau khi tìm kiếm bãi xe sẽ nhấn vào xem chi tiết, ko ai tìm kiếm rồi để đấy !
Các phần sau chia theo hướng chức năng như trong đặc tả yêu cầu !

Phân công nhiệm vụ use case cho các thành viên :

Hồng: Thêm xe cho bãi ( admin)
Thu: Tìm kiếm bãi xe + xem chi tiết xe trong bãi (user)
Đông: Thay đổi thông tin bãi xe ( admin)
Huy: Quản lí xe đang thuê  (admin)
Quang: Thuê xe ( user)
Sơn: Trả xe ( user)
Giang: Thay đổi thông tin xe ( admin)

Đặc tả use case: tương ứng !
Tài liệu SRS: Sẽ xử lí sau cùng, khi dự án đã được định hình rõ !

-------
Phần này thay đổi khá nhiều, vì trong quá trình bàn bạc trao đổi phát sinh nhiều vấn đề,
một số vấn đề bất khả thi khiến nhóm phải thay đổi chút ít trong dự án

Phân công công việc, đây là phân công sau cùng, các phân công trước đó đã được lưu trong nhật ký hoạt động
mình sẽ không đưa lên đây nữa 

Một lưu ý nhỏ, các bạn đã làm những gì thì cứ để nguyên trong thư mục cá nhân, sau này báo cáo cá nhân sẽ viết vào
Còn thư mục Group sẽ gửi bản hoàn thiện để nộp lại !
//Đông 
