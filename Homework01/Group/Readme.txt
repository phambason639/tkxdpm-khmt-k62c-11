Ý tưởng tạo use case:
Dựa vào các hành động của người dùng
Người dùng thường sau khi tìm kiếm bãi xe sẽ nhấn vào xem chi tiết, ko ai tìm kiếm rồi để đấy !
Các phần sau chia theo hướng chức năng như trong đặc tả yêu cầu !


Phân công nhiệm vụ use case cho các thành viên :


Hồng: Xem thông tin chi tiết xe trong bãi
Thu: Tìm kiếm bãi xe
Đông: Quản lí thông tin bãi xe
Huy: Quản lí xe đang thuê
Quang: Thuê xe
Sơn: Trả xe
Giang: Xem thông tin về xe đang thuê


Đặc tả use case: tương ứng !
Tài liệu SRS: Sẽ xử lí sau cùng, khi dự án đã được định hình rõ !