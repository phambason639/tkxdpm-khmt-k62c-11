package com.ebr.components.station.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.IDataManageController;
import com.ebr.components.abstractdata.gui.ADataEditDialog;

@SuppressWarnings("serial")
public class StationEditDialog extends ADataEditDialog<Station> {

	// private JTextField idField;
	private JTextField nameField;
	private JTextField addressField;
	private JTextField numberBikesField;
	private JTextField numberEBikesField;
	private JTextField numberTwinBikesField;
	private JTextField numberEmptyDocksField;

	public StationEditDialog(Station station, IDataManageController<Station> controller) {
		super(station, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel idLabel = new JLabel("Station Id");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);
		JLabel stationIdLabel = new JLabel();
		stationIdLabel.setText(t.getStationId());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(stationIdLabel, c);
		/*
		 * idField = new JTextField(25); idField.setText(t.getStationId()); c.gridx = 1;
		 * c.gridy = row; getContentPane().add(idField, c);
		 */

		row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Station Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(25);
		nameField.setText(t.getStationName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);

		row = getLastRowIndex();
		JLabel addressLabel = new JLabel("Station Address");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(addressLabel, c);
		addressField = new JTextField(25);
		addressField.setText(t.getStationAddress() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(addressField, c);

		row = getLastRowIndex();
		JLabel bikesLabel = new JLabel("Number Bikes");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(bikesLabel, c);
		numberBikesField = new JTextField(25);
		numberBikesField.setText(t.getNumberBikes() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberBikesField, c);

		row = getLastRowIndex();
		JLabel eBikesLabel = new JLabel("Number EBikes");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(eBikesLabel, c);
		numberEBikesField = new JTextField(25);
		numberEBikesField.setText(t.getNumberEBikes() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberEBikesField, c);

		row = getLastRowIndex();
		JLabel twinBikesLabel = new JLabel("Number TwinBikes");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(twinBikesLabel, c);
		numberTwinBikesField = new JTextField(25);
		numberTwinBikesField.setText(t.getNumberTwinBikes() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberTwinBikesField, c);

		row = getLastRowIndex();
		JLabel emptyDocksLabel = new JLabel("Number EmptyDocks");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(emptyDocksLabel, c);
		numberEmptyDocksField = new JTextField(25);
		numberEmptyDocksField.setText(t.getNumberEmptyDocks() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberEmptyDocksField, c);

	}

	public boolean checkValueNumber(int numberBikes, int numberEbikes, int numberTwinBikes, int numberEmptyDocks) {
		if (numberBikes < 0 || numberEbikes < 0 || numberTwinBikes < 0 || numberEmptyDocks < 0)
			return false;
		return true;
	}

	public void showNotice(boolean check) {
		JFrame notice = new JFrame();
		if (check == false)
			JOptionPane.showMessageDialog(notice, "Warning! Data invalid. Please re-input the data !");
	}

	@Override
	public Station getNewData() {
		
		boolean b = checkValueNumber(Integer.parseInt(numberBikesField.getText()),
				Integer.parseInt(numberEBikesField.getText()), Integer.parseInt(numberTwinBikesField.getText()),
				Integer.parseInt(numberEmptyDocksField.getText()));
		showNotice(b);
		
		// Station st = new Station();
		// st.setStationId(idField.getText());
		// t.setStationId(t.getStationId());
		t.setStationName(nameField.getText());
		t.setStationAddress(addressField.getText());
		t.setNumberBikes(Integer.parseInt(numberBikesField.getText()));
		t.setNumberEBikes(Integer.parseInt(numberEBikesField.getText()));
		t.setNumberTwinBikes(Integer.parseInt(numberTwinBikesField.getText()));
		t.setNumberEmptyDocks(Integer.parseInt(numberEmptyDocksField.getText()));
		return t;
	}
}
