package com.ebr.components.adminstation.controller;

import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.station.gui.AdminStationListPane;


public abstract class StationPageController extends ADataPageController<Station> {
	public StationPageController() {
		super();
	}
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new AdminStationListPane(this);
	}
	
	public abstract Station updateStation(Station station);

	
}
