package com.example.swing;

import java.awt.Dialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javafx.scene.layout.Background;

public class EkoBikeOrigin {

	public JFrame frame;

	public EkoBikeOrigin() {

		frame = new JFrame("EKO_BIKE Admin");
		// panel = (JPanel) getContentPane();
		Font titleFont = new Font("Arial", Font.PLAIN, 50);
		JLabel title = new JLabel("HỆ THỐNG QUẢN LÝ EKO_BIKE");
		title.setFont(titleFont);
		title.setBounds(360, 15, 1000, 100);
		frame.add(title);

		

		Font titleFont1 = new Font("Arial", Font.PLAIN, 30);
		JLabel title1 = new JLabel("Quản lý bãi xe");
		title1.setFont(titleFont1);
		title1.setBounds(600, 90, 800, 100);
		frame.add(title1);

		

		JLabel cityLabel = new JLabel("Chọn bãi xe: ");
		cityLabel.setBounds(100, 200, 300, 20);
		frame.add(cityLabel);

		

		String baiXe[] = { "BÃI XE 1", "BÃI XE 2", "BÃI XE 3", "BÃI XE 4", "BÃI XE 5" };
		JComboBox bx = new JComboBox(baiXe);
		bx.setBounds(100, 220, 300, 50);
		frame.add(bx);

		

		JButton btnXem = new JButton("XEM CHI TIẾT");
		btnXem.setBounds(450, 220, 180, 50);
		btnXem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				JPanel dialogXem = new JPanel();
				JOptionPane.showMessageDialog(dialogXem,
						"Tên: Bãi xe 1; \nVị trí: HN \nSố lượng xe:100 \nTình trạng: Tốt", "Bãi xe 1",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		frame.add(btnXem);

		

		JButton btnDoi = new JButton("THAY ĐỔI THÔNG TIN");
		btnDoi.setBounds(700, 220, 180, 50);
		frame.add(btnDoi);

		

		JButton btnQuanLy = new JButton("QUẢN LÝ XE TRONG BÃI");
		btnQuanLy.setBounds(950, 220, 180, 50);
		frame.add(btnQuanLy);

		

		JButton btnXoa = new JButton("XOÁ BÃI XE");
		btnXoa.setBounds(1200, 220, 180, 50);
		btnXoa.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				JPanel dialogXoa = new JPanel();
			
				// icon mặc định, tiêu đề tùy chỉnh
				int n = JOptionPane.showConfirmDialog(dialogXoa,
						"Bạn có muốn xoá không ?\nThao tác này không thể phục hồi ! \nVui lòng cân nhắc kĩ !",
						"Xác nhận xoá ", JOptionPane.YES_NO_OPTION);

			}
		});
		frame.add(btnXoa);

		
		/* Phần khung chính */
		frame.setLayout(null);
		frame.setSize(1500, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new EkoBikeOrigin();
	}
}