package com.ebr.components.media.controller;

import com.ebr.bean.Media;
import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.media.gui.ReturnVehicleListPane;

//tuong duong admin media page controller
public abstract class UserReturnVehiclePageController extends ADataPageController<Station> {
	public UserReturnVehiclePageController() {
		super();
	}
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new ReturnVehicleListPane(this);
	}
	
//	public abstract Station updateStation(Station station);
	//khong co updateStation, vi vay nen chuong trinh bi loi !
	
}
