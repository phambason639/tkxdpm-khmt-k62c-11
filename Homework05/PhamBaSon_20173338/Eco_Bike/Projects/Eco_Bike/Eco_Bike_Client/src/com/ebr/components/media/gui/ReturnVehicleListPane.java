package com.ebr.components.media.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.ebr.bean.Media;
import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.controller.IDataManageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataSinglePane;

import com.ebr.components.media.controller.AdminStationPageController;

@SuppressWarnings("serial")
public class ReturnVehicleListPane extends ADataListPane<Station>{
	
	public ReturnVehicleListPane(ADataPageController<Station> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Station> singlePane) {
		JButton button = new JButton("Return Vehicle");
		singlePane.addDataHandlingComponent(button);
		
		IDataManageController<Station> manageController = new IDataManageController<Station>() {
			@Override
			public void update(Station t) {
				if (controller instanceof AdminStationPageController) {
				Station newStation = ((AdminStationPageController) controller).updateStation(t);
					singlePane.updateData(newStation);
				}
			}

			@Override
			public void create(Station t) {
			}

			@Override
			public void read(Station t) {
			}

			@Override
			public void delete(Station t) {
				
			}
		};
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new StationEditDialog(singlePane.getData(), manageController);
			}
		});	
	}
}
