Các thành phần cần nộp
Report: cần có report chung cho toàn nhóm bao gồm (1) các phần trong tài liệu đặc tả
phần mềm SRS, (2) mô tả thiết kế phần mềm (biểu đồ gói, biểu đồ lớp phân tích, biểu
đồ lớp thiết kế chung toàn nhóm), và (3) nhật ký làm việc nhóm. Đồng thời mỗi cá nhân
cần có file report riêng bao gồm (1) đặc tả use case mình phụ trách, (2) các biểu đồ trình
tự cho lớp phân tích, (3) biểu đồ lớp phân tích, (4) biểu đồ gói, (5) biểu đồ trình tự cho
lớp thiết kế, (6) biểu đồ lớp thiết kế, (7) Mô tả kỹ thuật kiểm thử hộp đen hộp trắng đã
sử dụng và các test case thiết kế được. File report cá nhân chỉ chứa những thông tin làm
việc bởi chính cá nhân đó, liên quan tới use case cá nhân đó phụ trách. Tất cả các báo
cáo cần được in ra khi đi trình bày cho giảng viên.
• Projects: Chứa mã nguồn cuối cùng, bao gồm cả mã nguồn cho chương trình chính kèm
code kiểm thử đơn vị
• Readme.txt: Tổng kết toàn bộ phân công việc từ đầu đến cuối.

Báo cáo cá nhân thì không cần bìa xanh, nhưng dán gáy đàng hoàng, ngay ngắn
Báo cáo chính sẽ kèm 1,2 tờ phân công nhiệm vụ và nhật ký hoạt động, phần này mình đã lo cho nhóm rồi !

-------------------
Đây là mục lưu trữ bài làm chung của cả nhóm, kèm theo file nhật kí hoạt động, mục này tớ sẽ nhận ghi chép chủ yếu !!!
Các bạn có thể xem và sửa đổi nếu mình ghi chép thiếu xót !
// Đông 
// Link file sheet
https://docs.google.com/spreadsheets/u/0/d/1v3z0blnGQxm2jAix4bi6sVvVkq6X9q3SelcIL9vmHMw/htmlview?fbclid=IwAR0f-C4wHnMcxHlDGGptTZnc72EXpa6Qc15foi7phomEDLsbMIkHUU-xJKE