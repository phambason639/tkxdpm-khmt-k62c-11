package com.ebr.components.returnvehicle.controller;

import java.util.List;
import java.util.Map;

import com.ebr.bean.Station;
import com.ebr.components.media.controller.UserReturnVehiclePageController;
import com.ebr.components.media.gui.ReturnVehicleSearchPane;
import com.ebr.components.media.gui.ReturnVehicleSinglePane;
import com.ebr.serverapi.StationApi;

public class ReturnVehiclePageController extends UserReturnVehiclePageController{
	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {
		return new StationApi().getStation(searchParams);
	}
	
	@Override
	public ReturnVehicleSinglePane createSinglePane() {
		return new ReturnVehicleSinglePane();
	}
	
	@Override
	public ReturnVehicleSearchPane createSearchPane() {
		return new ReturnVehicleSearchPane();
	}
	
//	@Override
//	public Station updateStation(Station station) {
//		return new StationApi().updateStation((Station) station);
//	}
}
