package com.ebr.app.user;

import javax.swing.JPanel;
//controller cua user
import com.ebr.bean.Media;
import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.returnvehicle.controller.ReturnVehiclePageController;

public class OMSUserController {	
	public OMSUserController() {
	}
	
	public JPanel getReturnVehiclePage() {
		//ADataPageController<Station> controller = new StationManagerPageController();
		ReturnVehiclePageController controller = new ReturnVehiclePageController();
		return controller.getDataPagePane();
	}
}
