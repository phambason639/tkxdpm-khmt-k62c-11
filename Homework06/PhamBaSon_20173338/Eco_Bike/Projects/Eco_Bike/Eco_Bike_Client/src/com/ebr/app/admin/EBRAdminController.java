package com.ebr.app.admin;

import javax.swing.JPanel;

//controller cua admin
import com.ebr.bean.Media;
import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.station.controller.StationManagerPageController;

public class EBRAdminController {
	public EBRAdminController() {
	}
	
	public JPanel getStationManagerPage() {
		//ADataPageController<Station> controller = new StationManagerPageController();
		StationManagerPageController controller = new StationManagerPageController();
		return controller.getDataPagePane();
	}
}
