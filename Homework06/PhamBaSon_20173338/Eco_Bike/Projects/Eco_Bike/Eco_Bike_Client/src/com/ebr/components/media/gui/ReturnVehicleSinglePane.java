package com.ebr.components.media.gui;

import javax.swing.JLabel;

import com.ebr.bean.Station;
import com.ebr.components.abstractdata.gui.ADataSinglePane;
@SuppressWarnings("serial")
public class ReturnVehicleSinglePane extends ADataSinglePane<Station>{
	private JLabel labelId;
	private JLabel labelType;
	private JLabel labelName;
	private JLabel labelWeight;
	private JLabel labelLincensePlate;
	private JLabel labelCost;
	private JLabel labelStationId;
	private JLabel labelIsRental;
	
	public ReturnVehicleSinglePane() {
		super();
	}
		
	
	public ReturnVehicleSinglePane(Station station) {
		this();
		this.t = station;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelId = new JLabel();
		add(labelId, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelType = new JLabel();
		add(labelType, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelWeight = new JLabel();
		add(labelWeight, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLincensePlate = new JLabel();
		add(labelLincensePlate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationId = new JLabel();
		add(labelStationId, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelIsRental = new JLabel();
		add(labelIsRental, c);
	}
	
	
	@Override
	public void displayData() {
		labelId.setText("Vehicle Id: " + t.getStationId());
		labelType.setText("Vehicle Type: " + t.getStationName());
		labelName.setText("Vehicle Name: " + t.getStationAddress());
		labelWeight.setText("Vehicle weight: "+ t.getNumberBikes());
		labelLincensePlate.setText("Vehicle license plate: "+ t.getNumberEBikes());
		labelCost.setText("Vehicle cost: "+ t.getNumberTwinBikes());
		labelStationId.setText("Vehicle Station Id: "+ t.getNumberEmptyDocks());
	}
}
