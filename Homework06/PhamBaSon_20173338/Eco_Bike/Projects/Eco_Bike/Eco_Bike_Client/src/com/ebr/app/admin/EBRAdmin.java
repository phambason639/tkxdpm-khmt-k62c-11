package com.ebr.app.admin;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.*;

// khoi tao giao dien
@SuppressWarnings("serial")
public class EBRAdmin extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public EBRAdmin(EBRAdminController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
		//Font titleFont1 = new Font("Arial", Font.PLAIN, 1);
		//JTextField title1 = new JTextField("Admiaan");
		//title1.setFont(titleFont1);
		//title1.setBounds(600, 90, 800, 100);
		//tabbedPane.add(title1);
		
		// cac tab trang can thiet: gom quan ly bai xe, quan ly thong tin xe, quan ly xe dang thue
		JPanel stationPage = controller.getStationManagerPage();
		tabbedPane.addTab("Station Manager", null, stationPage, "Station Manager");
		//tabbedPane.addTab("Books", null, bookPage, "Books");
		
		tabbedPane.addTab("Vehicle Manager", null, new JPanel(), "Vehicle Manager");
		tabbedPane.addTab("Vehicle Renting", null, new JPanel(), "Vehicle Renting");
		//tabbedPane.addTab("Digital Video Discs", null, new JPanel(), "Digital Video Discs");

		//cac thuoc tinh co ban
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Eco Bike System for Administrator");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new EBRAdmin(new EBRAdminController());// bat dau chuong trinh
			}
		});
	}
}