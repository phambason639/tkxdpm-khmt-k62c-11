package com.ebr.app.admin;

import javax.swing.JPanel;

import com.ebr.components.station.controller.AdminStationPageController;

public class EBRAdminController {
	public EBRAdminController() {
	}
	
	public JPanel getStationPage() {
		//ADataPageController<Station> controller = new StationManagerPageController();
		AdminStationPageController controller = new AdminStationPageController();
		return controller.getDataPagePane();
	}
}
