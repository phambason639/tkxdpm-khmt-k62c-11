package com.ebr.components.station.controller;

import java.util.List;
import java.util.Map;

import com.ebr.bean.Station;
import com.ebr.components.adminstation.controller.StationPageController;
import com.ebr.components.station.gui.StationSearchPane;
import com.ebr.components.station.gui.StationSinglePane;
import com.ebr.serverapi.StationApi;

public class AdminStationPageController extends StationPageController{
	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {
		return new StationApi().getStation(searchParams);
	}
	
	@Override
	public StationSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public StationSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	@Override
	public Station updateStation(Station station) {
		return new StationApi().updateStation((Station) station);
	}
}
